<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\msds_customize\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Component\Render\FormattableMarkup;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
require 'vendor/authorizenet/authorizenet/autoload.php';

class SubscriptionController { 
	
	public function content($subscription_plan_id=null) {
		$userCurrent = \Drupal::currentUser();
		$user = \Drupal\user\Entity\User::load($userCurrent->id());
		$name = $user->getUsername();
		if (! $name) {	
			$response = new RedirectResponse('/user/login', 301);
			// drupal_set_message(t("Please login first"), 'error');
			$formatted_link = new FormattableMarkup(
				'<a href="@link">here</a>',
				[
				  '@link' => '/user/register',
				]
			  );
			drupal_set_message(t("Kindly login to purchase subscription if you are already registered with MSDS or else click @register_link to register", ['@register_link' => $formatted_link]), 'warning', TRUE);

			return $response->send();
		}

		// $nids = \Drupal::entityQuery('node')->condition('type','pricing_plans')->execute();
		// $nodes =  \Drupal\node\Entity\Node::loadMultiple($nids);

		$query = \Drupal::entityQuery('node')
		->condition('nid', $subscription_plan_id)
		->condition('status', 1)
		->condition('type','pricing_plans');

		$node_array = $query->execute();

		if($node_array):
			$node_id = array_values($node_array)[0];
			$GLOBALS['plan_id'] = $node_id;
		else:
			$response = new RedirectResponse('/user', 301);
			drupal_set_message(t("Please select valid plan"), 'error');
			return $response->send();
		endif;
		


		// foreach($nodes as $node):
		// 	//$plan_body['data'] = $node->get('body')->getValue()[0]['value'];
		// endforeach;

		$GLOBALS['plan_body'] = @$plan_body;
		// die;
		
		return array(
			// '#theme' => 'block__my_module',
			'#type' => 'markup',
			'#markup' => t('Subscription') . $subscription_plan_id,
			//'#plan_body' => @$plan_body
		);
	}

	function cancelSubscription()
	{
		$userCurrent = \Drupal::currentUser();

		$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

		$status = 0;
		$message = '';
		$data = []; 

		$subscriptionId = $_REQUEST['subscription_id'];		

		$cancleFromDb = $this->cancleSubscriptionInDb($subscriptionId);
		
		if(!$cancleFromDb){
			$response = new RedirectResponse('/user/' .$userCurrent->id(), 301);
			drupal_set_message('Subscription not cancelled', 'error');
			return $response->send();
		}

		
		$api_login_id = theme_get_setting('authorize_net_api_login_id', 'msds');
		$api_transaction_key = theme_get_setting('authorize_net_api_transaction_key', 'msds');

		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName($api_login_id);
		$merchantAuthentication->setTransactionKey($api_transaction_key);

    // Set the transaction's refId
		$refId = 'ref' . time();
		$request = new AnetAPI\ARBCancelSubscriptionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setSubscriptionId($subscriptionId);
		$controller = new AnetController\ARBCancelSubscriptionController($request);
		$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok"))
		{
			$successMessages = $response->getMessages()->getMessage();
			//echo "SUCCESS : " . $successMessages[0]->getCode() . "  " .$successMessages[0]->getText() . "\n";

				$tier1_user_role = MSDS_USER_ROLE_TIER1_SUBSCRIPTION;				
				$tier2_user_role = MSDS_USER_ROLE_TIER2_SUBSCRIPTION;
					
				$user->removeRole($tier1_user_role);
				$user->removeRole($tier2_user_role);
				$user->save();

			$status = 1;
			$message = $successMessages[0]->getText();
			drupal_set_message($message, 'success');

		}
		else
		{
			//echo "ERROR :  Invalid response\n";
			$errorMessages = $response->getMessages()->getMessage();
			//echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";

			$status = 0;
			$message = $errorMessages[0]->getText();
			drupal_set_message($message, 'error');

		}

		$response = new RedirectResponse('/user/' .$userCurrent->id() , 301);
		return $response->send();

		$response_array['status'] = $status;
		$response_array['message'] = $message;
		$response_array['data'] = $data;

		return $response_array;
	}

	public function cancleSubscriptionInDb($subscription_id)
	{
		$status = 0;
		$query = \Drupal::database()->update('user_subscriptions_master');
		$query->fields([
			'subscription_status' => 0,
		]);
		$query->condition('subscription_id', $subscription_id);
		if($query->execute()){
			$status = 1;
		}

		return $status;
	} 

	public function enableSubscriptionInDb($subscription_id)
	{
		$status = 0;
		$query = \Drupal::database()->update('user_subscriptions_master');
		$query->fields([
			'subscription_status' => 1,
		]);
		$query->condition('subscription_id', $subscription_id);
		if($query->execute()){
			$status = 1;
		}

		return $status;
	} 

}