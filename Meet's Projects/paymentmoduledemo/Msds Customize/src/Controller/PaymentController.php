<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\msds_customize\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
require 'vendor/authorizenet/authorizenet/autoload.php';
//define("AUTHORIZENET_LOG_FILE", "phplog");

class PaymentController {
	
	

	public function content() {
		require_once(drupal_get_path('theme', 'msds') ."/DataAccess/pricing_plan.php");
		// print_r($_REQUEST['subscription_plan_id']);
		// die;
		//$this->createCreditCardCheckoutTransaction($_REQUEST);
		//$this->createSubscription($_REQUEST);
		$user_array = [];
		$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

		$user_array['id'] = \Drupal::currentUser()->id();
		$user_array['website'] = $user->get('field_website')->value;
		$user_array['email'] = $user->get('mail')->value;
		$user_array['name'] = $user->get('name')->value;
		$user_array['uid']= $user->get('uid')->value;
		$user_array['authorize_profile_id']= $user->get('field_user_authorize_profile_id')->value;
		$user_array['authorize_payment_id']= $user->get('field_user_authorize_payment_id')->value;

		// $user_array['card_number']= '4111111111111111';
		// $user_array['card_expiry_data']= '2038-12';

		$user_array['card_number']= $_REQUEST['card_number'];
		$user_array['card_expiry_data']= $_REQUEST['card_expiry_date'];

		$subscription_array = [];
		$subscription_array['user'] = $user_array;
		$subscription_array['subscription'] = getSinglePricingPlanFromId($_REQUEST['subscription_plan_id']);

		if(!$user_array['authorize_profile_id']){

			$save_user_profile = $this->createCustomerProfile($subscription_array);


			if($save_user_profile['status']):
				

				//$user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

				$user->set('field_user_authorize_profile_id', $save_user_profile['data']['profile_id']);
				$user->set('field_user_authorize_payment_id', $save_user_profile['data']['payment_profile_id']);

				$user->save();
				sleep(10);
				// die;

			//sleep(10);
			// $transaction = $this->chargeCustomerProfile($save_user_profile['data']['profile_id'], $save_user_profile['data']['payment_profile_id'], 28);
			// die;
			else:
				print_r($save_user_profile['message']);
				return;
			endif;
			
		}

		$subscription_array['user']['profile_id'] = $user->get('field_user_authorize_profile_id')->value;
		$subscription_array['user']['payment_profile_id'] = $user->get('field_user_authorize_payment_id')->value;

		if($subscription_array['user']['profile_id'] && $subscription_array['user']['payment_profile_id']){
			
			$is_subscription_created = $this->createSubscription($subscription_array);
			// echo '<pre>';
			// print_r($is_subscription_created);
			// echo '</pre>';
			// die;
			if($is_subscription_created['status']):
// 				$conn = Database::getConnection();

// 				$Data = db_insert('table_name')
// ->fields(
// array(
// 'fid' => $form_state->getValue('img')[0],
// 'name' => $form_state->getValue('client_title'),
// 'description' => $form_state->getValue('short_desc'),
// 'changed' => time(),
// 'status' => '1',
// )
// )->execute();
				$add_subscription = db_insert('user_subscriptions_master')->fields(
					array(
						'user_id' => $subscription_array['user']['id'],
						'plan_id' => $subscription_array['subscription']['plan_id'],
						'plan_title' => $subscription_array['subscription']['title'],
						'plan_amount' => $subscription_array['subscription']['amount'],
						'subscription_status' => 1,
						'subscription_id' => $is_subscription_created['data']['subscription_id'],
						'subscription_transaction_payment_status' => 1,
						'subscription_start_date' => date('Y-m-d'),
						'subscription_end_date' => date('Y-m-d'),
						'subscription_transaction_payment_status' => 1,
						//'transaction_id' => '',
					)
				)->execute();


				if($subscription_array['subscription']['title'] == MSDS_PLAN_TIER1_TITLE){
					$user_role = MSDS_USER_ROLE_TIER1_SUBSCRIPTION;
					$remove_role = MSDS_USER_ROLE_TIER2_SUBSCRIPTION;
				}

				if($subscription_array['subscription']['title'] == MSDS_PLAN_TIER2_TITLE){
					$user_role = MSDS_USER_ROLE_TIER2_SUBSCRIPTION;
					$remove_role = MSDS_USER_ROLE_TIER1_SUBSCRIPTION;
				}

				$user->addRole($user_role);
				$user->removeRole($remove_role);
				$user->removeRole('existing_customer');
				$user->save();

				
				$mailManager = \Drupal::service('plugin.manager.mail');
				$module = 'msds';
				$key = 'subscription_successfull';
				// $to = 'testing123@yopmail.com';
				$to = $user->get('mail')->value;
				$langcode = \Drupal::currentUser()->getPreferredLangcode();
				$send = true;
				
				require_once(drupal_get_path('theme', 'msds') ."/DataAccess/pricing_plan.php");
				$params['pricing_plan_data'] = getSinglePricingPlanFromId($subscription_array['subscription']['plan_id']);
				
				
				$renderable = [
				  '#theme' => 'subscription_successfull_mail',
				  '#params' => $params,
				];
				$render_email = \Drupal::service('renderer')->render($renderable);
				
				// $mail = drupal_render($render_email);
				
				$parameters['body'] = $render_email;   
				$parameters['subject'] = t('You have successfully subscribed.');   
				
				$result = $mailManager->mail($module, $key, $to, $langcode, $parameters, NULL, $send);
				// if ($result['result'] !== true) {
				//   drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
				// }
				// else {
				//   drupal_set_message(t('Your message has been sent.'));
				// }


				$response = new RedirectResponse("/thank-you");
				$response->send();
				return;

			else:
				$error_message = t('Subscription not created. ') . $is_subscription_created['message'];
			endif;

			
		}else{
			$error_message = 'Authorize.net profile not found';
		}		
		
		if($error_message){
			drupal_set_message($error_message, 'error');
		}

		$page_content = '';

		return array(
			// '#theme' => 'block__my_module',
			'#type' => 'markup',
			'#markup' => $page_content,
			'#plan_body' => ''
		);
	}

	function createCustomerProfile($subscription_data){

		$refId = 'ref' . time();

		$api_login_id = theme_get_setting('authorize_net_api_login_id', 'msds');
		$api_transaction_key = theme_get_setting('authorize_net_api_transaction_key', 'msds');

	    /* Create a merchantAuthenticationType object with authentication details
	    retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName($api_login_id);
	    $merchantAuthentication->setTransactionKey($api_transaction_key);

		// Set credit card information for payment profile
	    $creditCard = new AnetAPI\CreditCardType();
	    $creditCard->setcardNumber($subscription_data['user']['card_number']);
	    $creditCard->setExpirationDate($subscription_data['user']['card_expiry_data']);
	    //$creditCard->setCardCode("142");

	    $paymentCreditCard = new AnetAPI\PaymentType();
	    $paymentCreditCard->setCreditCard($creditCard);

    	// Create the Bill To info for new payment type
	    $billTo = new AnetAPI\CustomerAddressType();
	    $billTo->setFirstName($subscription_data['user']['name']);
	    $billTo->setLastName($subscription_data['user']['name']);
		// $billTo->setCompany("Souveniropolis");
		// $billTo->setAddress("14 Main Street");
		// $billTo->setCity("Pecan Springs");
		// $billTo->setState("TX");
		// $billTo->setZip("44628");
		// $billTo->setCountry("USA");
		// $billTo->setPhoneNumber("888-888-8888");
		// $billTo->setfaxNumber("999-999-9999");

		// Create a customer shipping address
	    // $customerShippingAddress = new AnetAPI\CustomerAddressType();
	    // $customerShippingAddress->setFirstName("James");
	    // $customerShippingAddress->setLastName("White");
	    // $customerShippingAddress->setCompany("Addresses R Us");
	    // $customerShippingAddress->setAddress(rand() . " North Spring Street");
	    // $customerShippingAddress->setCity("Toms River");
	    // $customerShippingAddress->setState("NJ");
	    // $customerShippingAddress->setZip("08753");
	    // $customerShippingAddress->setCountry("USA");
	    // $customerShippingAddress->setPhoneNumber("888-888-8888");
	    // $customerShippingAddress->setFaxNumber("999-999-9999");

	    //  // Create an array of any shipping addresses
	    // $shippingProfiles[] = $customerShippingAddress; 

	    // Create a new CustomerPaymentProfile object
	    $paymentProfile = new AnetAPI\CustomerPaymentProfileType();
	    $paymentProfile->setCustomerType('individual');
	    $paymentProfile->setBillTo($billTo);
	    $paymentProfile->setPayment($paymentCreditCard);
	    $paymentProfile->setDefaultpaymentProfile(true);
	    $paymentProfiles[] = $paymentProfile;

		 // Create a new CustomerProfileType and add the payment profile object
	    $customerProfile = new AnetAPI\CustomerProfileType();
	    $customerProfile->setDescription("Customer");
	    $customerProfile->setMerchantCustomerId("M_" . time());
	    $customerProfile->setEmail($subscription_data['user']['email']);
	    $customerProfile->setpaymentProfiles($paymentProfiles);
	    // $customerProfile->setShipToList($shippingProfiles);


    // Assemble the complete transaction request
	    $request = new AnetAPI\CreateCustomerProfileRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setRefId($refId);
	    $request->setProfile($customerProfile);

    // Create the controller and get the response
	    $controller = new AnetController\CreateCustomerProfileController($request);
	    // $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
	    $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);

	    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
	    	$paymentProfiles = $response->getCustomerPaymentProfileIdList();

	    	$status = 1;
	    	$message = t('Customer profile created succesfully.');
	    	$data = [];
	    	$data['profile_id'] = $response->getCustomerProfileId();
	    	$data['payment_profile_id'] = $paymentProfiles[0];
			// echo "Succesfully created customer profile : " . $response->getCustomerProfileId() . "\n";
			// $paymentProfiles = $response->getCustomerPaymentProfileIdList();
			// echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "\n";
	    } else {

	    	$errorMessages = $response->getMessages()->getMessage();

	    	$status = 0;
	    	$message = t('Customer profile not created. ' . $errorMessages[0]->getText() );
	    	$data = [];
			// echo "ERROR :  Invalid response\n";
			// $errorMessages = $response->getMessages()->getMessage();
			// echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";
	    }

	    $response_array = [];
	    $response_array['status'] = $status;
	    $response_array['message'] = $message;
	    $response_array['data'] = $data;

	    return $response_array;
	}


	function createSubscription($subscription_data){

		$status = 0;
		$message = '';
		$data = [];
		$response_array = [];

		$api_login_id = theme_get_setting('authorize_net_api_login_id', 'msds');
		$api_transaction_key = theme_get_setting('authorize_net_api_transaction_key', 'msds');

	    /* Create a merchantAuthenticationType object with authentication details
	    retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName($api_login_id);
	    $merchantAuthentication->setTransactionKey($api_transaction_key);
	    
	    // Set the transaction's refId
	    $refId = 'ref' . time();

	    // Subscription Type Info
	    $subscription = new AnetAPI\ARBSubscriptionType();
	    $subscription->setName($subscription_data['subscription']['title']);

	    $interval = new AnetAPI\PaymentScheduleType\IntervalAType();

	    $intervalLength  = 12;
	    $intervalUnit  = 'months';
	    if($subscription_data['subscription']['plan_duration'] === 'Monthly'){
	    	$intervalLength  = 1;
	    	$intervalUnit  = 'months';
	    }

	    $interval->setLength($intervalLength);
	    $interval->setUnit($intervalUnit);

	    $paymentSchedule = new AnetAPI\PaymentScheduleType();
	    $paymentSchedule->setInterval($interval);
	    $paymentSchedule->setStartDate(new \DateTime(date('Y-m-d')));
	    $paymentSchedule->setTotalOccurrences("12");
	    $paymentSchedule->setTrialOccurrences("0");



	    $subscription->setPaymentSchedule($paymentSchedule);
	    $subscription->setAmount($subscription_data['subscription']['amount']);
	    $subscription->setTrialAmount("0.00");


	    
	    // $creditCard = new AnetAPI\CreditCardType();
	    // // $creditCard->setCardNumber("4111111111111111");
	    // // $creditCard->setExpirationDate("2038-12");

	    // $creditCard->setCardNumber($subscription_data['user']['card_number']);
	    // $creditCard->setExpirationDate($subscription_data['user']['card_expiry_data']);

	    // $payment = new AnetAPI\PaymentType();
	    // $payment->setCreditCard($creditCard);
	    // $subscription->setPayment($payment);



	    $profile = new AnetAPI\CustomerProfileIdType();
	    $profile->setCustomerProfileId($subscription_data['user']['profile_id']);
	    $profile->setCustomerPaymentProfileId($subscription_data['user']['payment_profile_id']);

  		// $profile->setCustomerProfileId('1911554743');
	   //  $profile->setCustomerPaymentProfileId('1826297251');

	    // var_dump($profile);
	    // // var_dump($subscription_data['user']['payment_profile_id']);

	    // die;



	    $subscription->setProfile($profile);

	    $order = new AnetAPI\OrderType();
	    //$order->setInvoiceNumber("1234354");        
	    $order->setDescription($subscription_data['subscription']['title']); 
	    $subscription->setOrder($order); 
	    
	    // $billTo = new AnetAPI\NameAndAddressType();
	    // $billTo->setFirstName($subscription_data['user']['name']);
	    // $billTo->setLastName($subscription_data['user']['name']);

	    // $subscription->setBillTo($billTo);

	    $request = new AnetAPI\ARBCreateSubscriptionRequest();
	    $request->setmerchantAuthentication($merchantAuthentication);
	    $request->setRefId($refId);
	    $request->setSubscription($subscription);
	    $controller = new AnetController\ARBCreateSubscriptionController($request);

	    // $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
	    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);

	    if (($response != null) && ($response->getMessages()->getResultCode() == "Ok") )
	    {
	    	$status = 1;
	    	$message = 'Subscription successful.';
	    	$data['subscription_id'] = $response->getSubscriptionId();
	    	//echo "SUCCESS: Subscription ID : " . $response->getSubscriptionId() . "\n";
	    }
	    else
	    {
	    	// echo "ERROR :  Invalid response\n";
	    	$errorMessages = $response->getMessages()->getMessage();
	    	// echo "Response : " . $errorMessages[0]->getCode() . "  " .$errorMessages[0]->getText() . "\n";

	    	$status = 0;
	    	$message = 'Subscription failed. Error: ' . $errorMessages[0]->getCode() . ' '. $errorMessages[0]->getText();
	    	
	    }

	    $response_array['status'] = $status;
	    $response_array['message'] = $message;
	    $response_array['data'] = $data;

	    return $response_array;
	}

	public function chargeCustomerProfile($profileid, $paymentprofileid, $amount)
	{
		$api_login_id = theme_get_setting('authorize_net_api_login_id', 'msds');
		$api_transaction_key = theme_get_setting('authorize_net_api_transaction_key', 'msds');

	    /* Create a merchantAuthenticationType object with authentication details
	    retrieved from the constants file */
	    $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
	    $merchantAuthentication->setName($api_login_id);
	    $merchantAuthentication->setTransactionKey($api_transaction_key);

    // Set the transaction's refId
	    $refId = 'ref' . time();

	    $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
	    $profileToCharge->setCustomerProfileId($profileid);
	    $paymentProfile = new AnetAPI\PaymentProfileType();
	    $paymentProfile->setPaymentProfileId($paymentprofileid);
	    $profileToCharge->setPaymentProfile($paymentProfile);

	    $transactionRequestType = new AnetAPI\TransactionRequestType();
	    $transactionRequestType->setTransactionType( "authCaptureTransaction"); 
	    $transactionRequestType->setAmount($amount);
	    $transactionRequestType->setProfile($profileToCharge);

	    $request = new AnetAPI\CreateTransactionRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setRefId( $refId);
	    $request->setTransactionRequest( $transactionRequestType);
	    $controller = new AnetController\CreateTransactionController($request);
	    // $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
	    $response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);

	    if ($response != null)
	    {
	    	if($response->getMessages()->getResultCode() == 'Ok')
	    	{
	    		$tresponse = $response->getTransactionResponse();

	    		if ($tresponse != null && $tresponse->getMessages() != null)   
	    		{
	    			echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
	    			echo  "Charge Customer Profile APPROVED  :" . "\n";
	    			echo " Charge Customer Profile AUTH CODE : " . $tresponse->getAuthCode() . "\n";
	    			echo " Charge Customer Profile TRANS ID  : " . $tresponse->getTransId() . "\n";
	    			echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n"; 
	    			echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
	    		}
	    		else
	    		{
	    			echo "Transaction Failed \n";
	    			if($tresponse->getErrors() != null)
	    			{
	    				echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
	    				echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";            
	    			}
	    		}
	    	}
	    	else
	    	{
	    		echo "Transaction Failed \n";
	    		$tresponse = $response->getTransactionResponse();
	    		if($tresponse != null && $tresponse->getErrors() != null)
	    		{
	    			echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
	    			echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";                      
	    		}
	    		else
	    		{
	    			echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
	    			echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
	    		}
	    	}
	    }
	    else
	    {
	    	echo  "No response returned \n";
	    }

	    return $response;
	}



	public function createCreditCardCheckoutTransaction() {
		$api_login_id = theme_get_setting('authorize_net_api_login_id', 'msds');
		$api_transaction_key = theme_get_setting('authorize_net_api_transaction_key', 'msds');

		$amount = 100;
    // Common setup for API credentials
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName($api_login_id);
		$merchantAuthentication->setTransactionKey($api_transaction_key);
		$refId = 'ref' . time();
    // Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber("4111111111111111");
		$creditCard->setExpirationDate("1226");
		$creditCard->setCardCode("123");
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);
		$order = new AnetAPI\OrderType();
		$order->setDescription("New Item");
    //create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("authCaptureTransaction");
		$transactionRequestType->setAmount($amount);
		$transactionRequestType->setOrder($order);
		$transactionRequestType->setPayment($paymentOne);
    //Preparing customer information object
		$cust = new AnetAPI\CustomerAddressType();
		$cust->setFirstName('dev');
		$cust->setLastName('dev');
		$cust->setAddress('dev');
		$cust->setCity('dev');
		$cust->setState('dev');
		$cust->setCountry('dev');
		$cust->setZip('123789654');
		$cust->setPhoneNumber('9876543210');
		$cust->setEmail("abc@test.com");
		$transactionRequestType->setBillTo($cust);
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest($transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		// $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
		if ($response != null) {
			if ($response->getMessages()->getResultCode() == 'Ok') {
				$tresponse = $response->getTransactionResponse();
				if ($tresponse != null && $tresponse->getMessages() != null) {
					echo " Transaction Response code : " . $tresponse->getResponseCode() . "\n";
					echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
					echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
					echo " Code : " . $tresponse->getMessages()[0]->getCode() . "\n";
					echo " Description : " . $tresponse->getMessages()[0]->getDescription() . "\n";
				} else {
					echo "Transaction Failed \n";
					if ($tresponse->getErrors() != null) {
						echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
						echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
					}
				}
			} else {
				echo "Transaction Failed \n";
				$tresponse = $response->getTransactionResponse();
				if ($tresponse != null && $tresponse->getErrors() != null) {
					echo " Error code  : " . $tresponse->getErrors()[0]->getErrorCode() . "\n";
					echo " Error message : " . $tresponse->getErrors()[0]->getErrorText() . "\n";
				} else {
					echo " Error code  : " . $response->getMessages()->getMessage()[0]->getCode() . "\n";
					echo " Error message : " . $response->getMessages()->getMessage()[0]->getText() . "\n";
				}
			}
		} else {
			echo "No response returned \n";
		}
		return $response;
	}

	public function thankYou(){
		return array(
			'#type' => 'markup',
			'#markup' => t('Thank You')
		);
	}
}