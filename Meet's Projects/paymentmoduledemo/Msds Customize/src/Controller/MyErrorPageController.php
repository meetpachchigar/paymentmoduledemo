<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\msds_customize\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MyErrorPageController { 
	
	public function on404() {
		return array(
			'#theme' => 'page__404',
			'#type' => 'markup',
			'#markup' => t('404') 
		);
	}

}