<?php
/**
 * @file
 * Contains \Drupal\msds_customize\Form\ResumeForm.
 */
namespace Drupal\msds_customize\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityFormBuilder;

class ResumeForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_a_msds_record_to_the_databas';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    //  $node = \Drupal::entityTypeManager()->getStorage('node')->create([
    //   'type' => 'add_a_msds_record_to_the_databas',
    // ]);
    // return \EntityFormBuilder::getForm($node);

    $form = array(
      '#attributes' => array('enctype' => 'multipart/form-data'),
    );

    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Product Name:'),
      '#required' => TRUE,
    );

    $form['manufacturer_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Manufacturer Name:'),
      '#required' => TRUE,
    );

    $form['msds_type'] = array (
      '#type' => 'select',
      '#title' => ('Type'),
      '#options' => array(
        '' => t('Please select sds type'),
        'MSDS' => t('MSDS'),
        'SDS' => t('SDS'),
      ),
      '#required' => TRUE,
    );    

    $form['search_reference'] = array(
      '#type' => 'textfield',
      '#title' => t('Search Reference: Enter synonyms, trade names or additional information found in section 1')
    );

    // $form['file_upload_details'] = array(
    //   '#markup' => t('Safety Data Sheet (MSDS) PDF File'),
    // );

    $validators = array(
      'file_validate_extensions' => array('pdf'),
      'file_validate_size' => array(6 * 1024 * 1024),
    );
    $form['msds_file'] = array(
      '#type' => 'managed_file',
      '#name' => 'msds_file',
      '#title' => t('Safety Data Sheet (MSDS) PDF File *'),
      '#size' => 20,
      '#description' => t('PDF format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://msds/',
      '#required' => TRUE,
    );


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // if (strlen($form_state->getValue('candidate_number')) < 10) {
    //   $form_state->setErrorByName('candidate_number', $this->t('Mobile number is too short.'));
    // }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $node = entity_create('node', array(
     'type' => 'add_a_msds_record_to_the_databas',
     'title' =>$form_state->getValue('title'),
     'body' => array(
       'value' =>'The body of the content',
       'format' => 'full_html',
     ),
     'field_manufacturer_'=>$form_state->getValue('manufacturer_name'),
     'field__is_the_record_a_material_'=>$form_state->getValue('msds_type'),
     'field_safety_data_sheet_msds_pdf'=>$form_state->getValue('msds_file')[0],
     'field_synonyms_'=>$form_state->getValue('search_reference'),
   )
  );

    $is_msds_saved = $node->save();

    if($is_msds_saved){
      drupal_set_message(t('(M)sds Uploaded successfully.'));
    }else{
      drupal_set_message(t('(M)sds not uploaded.'));
    }

  }
}