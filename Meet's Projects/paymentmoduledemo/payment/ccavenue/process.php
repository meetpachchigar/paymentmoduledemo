<?php
#include required file
include_once("../../../includes/common.php");
include_once('adler32.php');
include_once('Aes.php');
require_once($physical_path['DB_Access']. 'Order.php');
require_once($physical_path['DB_Access']. 'PaymentModule.php');
	
#prepare object
$objOrder			=	new Order();
$objPaymentModule	=	new PaymentModule();

if(!empty($_GET['TID']) && !empty($_GET['OID']))
{
	#Define Variable
	$orderId	=	$_GET['OID'];
	$tokenID	=	$_GET['TID'];
	
	#get order information base on order id and token
	$rsOrderInfo	=	$objOrder->getInfoByParam(" order_id = ".$orderId." AND order_token = '".$tokenID."'");
	
	#check information available or not
	#IF available then prepare data for paypal checkout
	if(!empty($rsOrderInfo))
	{
		if($rsOrderInfo['order_cust_ship_city'] != '' && $rsOrderInfo['order_cust_ship_city'] != -1)
		{
			$rsCityInfo		=	$objCity->getInfoById($rsOrderInfo['order_cust_ship_city']);
			$rsOrderInfo['order_cust_ship_city']	=	$rsCityInfo['city_name'];
		}
		else
		{
			$rsOrderInfo['order_cust_ship_city']	=	$rsOrderInfo['order_cust_ship_other_city'];
		}
		
		$rsOrderInfo['order_cust_bill_country']	=	$objCountry->getName($rsOrderInfo['order_cust_bill_country']);
		$rsOrderInfo['order_cust_ship_country']	=	$objCountry->getName($rsOrderInfo['order_cust_ship_country']);
		
	
		#get ccavenue config info
		$rspayInfo		= $objPaymentModule->getPaymentConfig('CCAvenue');
		
		#Iterate throught loop
		foreach($rspayInfo as $key=>$val)
			$PayInfo[$val['config_name']]=$val['config_value'];
		
		#set return url	
		$returnurl 		= $virtual_path['Site_Root'].'mpayment/ccavenue/process.php';
		
		#set working key
		$working_key 	= $PayInfo['avenue_key'];
		
		#Define checksum for ccavenue
		//$ccavenuechecksum = getchecksum($PayInfo['avenue_id'],$rsOrderInfo['order_total'],$rsOrderInfo['order_id'],$returnurl,$working_key);
		$ccavenuechecksum = getchecksum($PayInfo['avenue_id'],$rsOrderInfo['order_total'],$rsOrderInfo['order_no'],$returnurl,$working_key);
		//$ccavenuechecksum = getchecksum($PayInfo['avenue_id'],1,$rsOrderInfo['order_id'],$returnurl,$working_key);
		
		
		$merchant_data	=	'Merchant_Id='.$PayInfo['avenue_id'].'&Amount='.$rsOrderInfo['order_total'].'&Order_Id='.$rsOrderInfo['order_no'].'&Redirect_Url='.$returnurl.'&billing_cust_name='.$rsOrderInfo['order_cust_bill_name'].'&billing_cust_address='.$rsOrderInfo['order_cust_bill_address'].'&billing_cust_country='.$rsOrderInfo['order_cust_bill_country'].'&billing_cust_state='.$rsOrderInfo['order_cust_bill_state'].'&billing_cust_city='.$rsOrderInfo['order_cust_bill_city'].'&billing_zip_code='.$rsOrderInfo['order_cust_bill_zip'].'&billing_cust_tel='.$rsOrderInfo['order_cust_bill_mbl'].'&billing_cust_email='.$rsOrderInfo['order_cust_bill_email'].'&delivery_cust_name='.$rsOrderInfo['order_cust_ship_name'].'&delivery_cust_address='.$rsOrderInfo['order_cust_ship_address'].'&delivery_cust_country='.$rsOrderInfo['order_cust_ship_country'].'&delivery_cust_state='.$rsOrderInfo['order_cust_ship_state'].'&delivery_cust_city='.$rsOrderInfo['order_cust_ship_city'].'&delivery_zip_code='.$rsOrderInfo['order_cust_ship_zip'].'&delivery_cust_tel='.$rsOrderInfo['order_cust_ship_mbl'].'&billing_cust_notes='.$rsOrderInfo['order_comment'].'&Checksum='.$ccavenuechecksum;

		#set merchant data
	//$merchant_data	=	'Merchant_Id='.$PayInfo['avenue_id'].'&Amount='.$rsOrderInfo['order_total'].'&Order_Id='.$rsOrderInfo['order_id'].'&Redirect_Url='.$returnurl.'&billing_cust_name='.$rsOrderInfo['order_cust_bill_name'].'&billing_cust_address='.$rsOrderInfo['order_cust_bill_address'].'&billing_cust_country='.$rsOrderInfo['order_cust_bill_country'].'&billing_cust_state='.$rsOrderInfo['order_cust_bill_state'].'&billing_cust_city='.$rsOrderInfo['order_cust_bill_city'].'&billing_zip_code='.$rsOrderInfo['order_cust_bill_zip'].'&billing_cust_tel='.$rsOrderInfo['order_cust_bill_telephone'].'&billing_cust_email='.$rsOrderInfo['order_cust_bill_email'].'&delivery_cust_name='.$rsOrderInfo['order_cust_ship_name'].'&delivery_cust_address='.$rsOrderInfo['order_cust_ship_address'].'&delivery_cust_country='.$rsOrderInfo['order_cust_ship_country'].'&delivery_cust_state='.$rsOrderInfo['order_cust_ship_state'].'&delivery_cust_city='.$rsOrderInfo['order_cust_ship_city'].'&delivery_zip_code='.$rsOrderInfo['order_cust_ship_zip'].'&delivery_cust_tel='.$rsOrderInfo['order_cust_ship_telephone'].'&billing_cust_notes='.$rsOrderInfo['order_comment'].'&Checksum='.$ccavenuechecksum;

		#set encrypt data
		$encrypted_data	=	encrypt($merchant_data,$working_key);

		#redirect url
		$ccavenueurl =	"https://www.ccavenue.com/shopzone/cc_details.jsp?Merchant_Id=".$PayInfo['avenue_id']."&encRequest=".$encrypted_data;
?>
<html>
<body>
<form method="post" name="redirect" action="http://www.ccavenue.com/shopzone/cc_details.jsp"> 
	<?php
		echo "<input type=hidden name=encRequest value=$encrypted_data>";
		echo "<input type=hidden name=Merchant_Id value=".$PayInfo['avenue_id'].">";
	?>
</form>
<script language='javascript'>document.redirect.submit();</script>
</body>
</html>
<?php
	}
	else
	{
		echo 'Illegal access!!';	
		die;
	}
}
# If pay by CCAvenue	
elseif(!empty($_REQUEST['Checksum']))
{
	$rsConfig	= $objPaymentModule->getPaymentConfig('CCAvenue');
	  
	# For CCAvenue checksum count		 
	foreach($rsConfig as $key=>$val)
	{
		$PayInfo[$val['config_name']]=$val['config_value'];
	}		

	$workingKey		= 	$PayInfo['avenue_key'];//Working Key should be provided here.
	$encResponse	=	$_POST["encResponse"];	//This is the response sent by the CCAvenue Server

	$rcvdString		=	decrypt($encResponse,$workingKey);//AES Decryption used as per the specified working key.
	$AuthDesc 		= 	$_REQUEST['AuthDesc'];
	$MerchantId 	= 	$_REQUEST['Merchant_Id'];
	$order_no		= $_REQUEST['Order_Id'];
	$order_id		= substr($_REQUEST['Order_Id'],8);
	/*$order_id		= 	$_REQUEST['Order_Id'];
	$order_id		= 	$_REQUEST['Order_Id'];*/
	$Amount			=	$_REQUEST['Amount'];
	$Checksum		=	$_REQUEST['Checksum'];
	$nb_order_no	=	$_REQUEST['nb_order_no'];
	$veriChecksum	=	false;

	$decryptValues	=	explode('&', $rcvdString);
	$dataSize		=	sizeof($decryptValues);

	#******************************    Messages based on Checksum & AuthDesc   **********************************
	for($i = 0; $i < $dataSize; $i++) 
	{
		$information=explode('=',$decryptValues[$i]);
		if($i==0)	$MerchantId=$information[1];	
		if($i==1)	$OrderId=$information[1];
		if($i==2)	$Amount=$information[1];	
		if($i==3)	$AuthDesc=$information[1];
		if($i==4)	$Checksum=$information[1];	
	}

	$rcvdString=$MerchantId.'|'.$OrderId.'|'.$Amount.'|'.$AuthDesc.'|'.$workingKey;
	$veriChecksum=verifyChecksum(genchecksum($rcvdString), $Checksum);

	$rsOrder = $objOrder->getInfoById($order_id);
	
	if($nb_order_no!='' && $AuthDesc=="Y" && doubleval($rsOrder['order_total'])==doubleval($Amount))
	{
		$objOrder->InsertComment($order_id,	'', 0, '', 'Completed', '', 	1, 1, '', serialize($_REQUEST));
		echo 'Thank you for shopping with us. Your credit card has been charged and your transaction is successful. We will be shipping your order to you soon.';
		
		echo '<script>parent.location="http://m.xxxxxxxxx.com/paythanku.html";</script>';
		exit(0);
	}
	else if($nb_order_no!='' && $AuthDesc=="B")
	{
		$objOrder->InsertComment($order_id,	'', 0, '', 'Pending', '', 	1, 1, '', serialize($_REQUEST));
		
		echo 'Thank you for shopping with us.We will keep you posted regarding the status of your order through e-mail.';
		
		echo '<script>parent.location="http://m.xxxxxxxxx.com/paythanku.html";</script>';
		exit(0);
	}
	else if($nb_order_no!='' && $AuthDesc=="N")
	{
		$objOrder->InsertComment($order_id,	'', 0, '', 'Canceled', '', 1, 1, '', serialize($_REQUEST));
		
		echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
		exit();
	}
	else
	{
		$objOrder->InsertComment($order_id,	'', 0, '', 'Canceled', '',  1, 1,'', serialize($_REQUEST));
		
		echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
		exit();
	}
}
else{
	echo '<script>parent.location="http://m.xxxxxxxxx.com/cart.html";</script>';
	exit();
}

?>
