<?php
include_once("../../../includes/common.php");
include_once("../../../includes/paypal_config.php");
include_once("paypal.class.php");
require_once($physical_path['DB_Access']. 'Order.php');
$objOrder			=	new Order();
$PayPalReturnURL    = 'http://xxxxxxxxx.com/mpayment/paypal/process.php?OID='.$_GET['OID'].'&mod=web'; #Point to thank you page
$PayPalCancelURL    = 'http://xxxxxxxxx.com/mpayment/paypal/process.php?OID='.$_GET['OID'].'&mod=web';

//$PayPalReturnURL    = 'http://xxxxxxxxx.local/mpayment/paypal/process.php?OID='.$_GET['OID'].'&mod=web'; #Point to thank you page
//$PayPalCancelURL    = 'http://xxxxxxxxx.local/mpayment/paypal/process.php?OID='.$_GET['OID'].'&mod=web';
/*if(empty($_GET['TID']) && empty($_GET['OID']))
{
	echo '';
	die;
}
else
{
*/
if(!empty($_GET['TID']) && !empty($_GET['OID']))
{
	#Define Variable
	$orderId	=	$_GET['OID'];
	$tokenID	=	$_GET['TID'];
	
	#include required file
	#require_once($physical_path['DB_Access']. 'Order.php');
	require_once($physical_path['DB_Access']. 'PaymentModule.php');
	
	#prepare object
	#$objOrder			=	new Order();
	$objPaymentModule	=	new PaymentModule();

	#get order information base on order id and token
	$rsOrderInfo	=	$objOrder->getInfoByParam(" order_id = ".$orderId." AND order_token = '".$tokenID."'");
	
	#check information available or not
	#IF available then prepare data for paypal checkout
	if(!empty($rsOrderInfo))
	{
		#Get CurrencyInfo
		$rsCurInfo = $objCurrencyModule->getKeyValueArray(" and currency_visible = 'Yes' and currency_id=".$rsOrderInfo['order_currency_code']);
		
		#setPaypalCurrency
		if(!empty($rsCurInfo[$rsOrderInfo['order_currency_code']]))
			$PayPalCurrencyCode	=	$rsCurInfo[$rsOrderInfo['order_currency_code']];
		else
			$PayPalCurrencyCode	=	'USD';
		
		#Get paypal mode info i.e testing or production mode
		$rspayInfo	=	$objPaymentModule->getInfoById($rsOrderInfo['order_pay_method']);
		$paymentmode	=	$objPaymentModule->getPaymentConfigValue($rspayInfo['module_key'],'paypal_testmode');	
		
		#set paypalmode 
		if($paymentmode['config_value']=='Y')
			$PayPalMode	=	'sandbox';
		
		$paypalmode = ($PayPalMode=='sandbox') ? '.sandbox' : '';
		
		#Tax cost,Discount,Shipping cost
		$TotalTaxAmount 	= round($rsOrderInfo['order_tax_amt']/$rsOrderInfo['order_currency_rate'],2);  #Sum of tax for all items in this order.
		
		$ShippinCost 		= round($rsOrderInfo['order_Shipping_charges']/$rsOrderInfo['order_currency_rate'],2);#Shipping Cost.
		
		#define variable
		$paypal_data 	=	'';
		$ItemTotalPrice = 0;
		$Discount		=	0;
		#get Order Product info
		$rsProductInfo	=	$objOrder->getProductInformation($orderId);
		
		#Iterate throught order product info
		foreach($rsProductInfo as $key=>$Record)
		{
			$price	=	round($Record['op_prod_price']/$rsOrderInfo['order_currency_rate'],2);
			#assign info
			$paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$key.'='.urlencode($Record['op_prod_title']);
        	$paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$key.'='.urlencode($Record['op_prod_code']);
        	$paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$key.'='.urlencode($price);		
			$paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$key.'='. urlencode($Record['op_prod_qty']);
			
			#Subtotal => item price X quantity
        	$subtotal = ($price*$Record['op_prod_qty']);
        	
        	#total price
        	$ItemTotalPrice = $ItemTotalPrice + $subtotal;
        	
        	#Discount
        	$Discount	=	$Discount	+	$Record['op_coupon_discount'];
        	
        	#create items for session
			$paypal_product['items'][] = array(	'itm_name'=>$Record['op_prod_title'],
												'itm_price'=>$price,
												'itm_code'=>$Record['op_prod_code'], 
												'itm_qty'=>$Record['op_prod_qty']
											);
		}
		//echo $rsOrderInfo['order_ccode_apply_to'];die;
		if($rsOrderInfo['order_ccode_apply_to'] == 'subtotal'){
			//echo 'ad';die;
			$Discount1			= -round($rsOrderInfo['order_coupon_discount']/$rsOrderInfo['order_currency_rate'],2); #Discount for this order. Specify this as negative number. 
			//echo $rsOrderInfo['order_coupon_discount'];die;
		}
		else{
			$Discount1			=	-round($Discount/$rsOrderInfo['order_currency_rate'],2);
		}
		
		//echo $Discount1;die;
		//echo $ItemTotalPrice;die;
		$ItemTotalPrice1	=	$ItemTotalPrice;
		
		#Grand total including all tax, shipping cost and discount
		$GrandTotal = round(($ItemTotalPrice1 + $TotalTaxAmount + $ShippinCost + $Discount1),2);
	//	echo $GrandTotal;die;
		$paypal_product['assets'] = array('tax_total'=>$TotalTaxAmount, 
										'shippin_cost'=>$ShippinCost,
										'discount'=>$Discount1,
										'grand_total'=>$GrandTotal
									);
		
		#create session array for later use
		$_SESSION["paypal_products"] = $paypal_product;
		
		#Parameters for SetExpressCheckout, which will be sent to PayPal
		$padata = 	'&METHOD=SetExpressCheckout'.
					'&RETURNURL='.urlencode($PayPalReturnURL ).
					'&CANCELURL='.urlencode($PayPalCancelURL).
					'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
					$paypal_data.				
					'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice1).
					'&PAYMENTREQUEST_0_TAXAMT='.urlencode($TotalTaxAmount).
					'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($ShippinCost).
					'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($Discount1).
					'&PAYMENTREQUEST_0_AMT='.urlencode($GrandTotal).
					'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode).
					'&ALLOWNOTE=1';
		
		
		#We need to execute the "SetExpressCheckOut" method to obtain paypal token
		$paypal	= new MyPayPal();
	
		$httpParsedResponseAr = $paypal->PPHttpPost('SetExpressCheckout', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
		
		/*echo '<pre>';
		print_r($padata);die;*/
		
		#Respond according to message we receive from Paypal
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"]))
		{
			#Redirect user to PayPal store with Token received.
			$paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
			@header('Location: '.$paypalurl);
		}
		else
		{
			#Show error message
			echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
			echo '<pre>';
			print_r($httpParsedResponseAr);
			echo '</pre>';
		}
		
		
	}
	#IF no information available
	else
	{
		echo 'Illegal access!!';	
		die;
	}
}
#Paypal redirects back to this page using ReturnURL, We should receive TOKEN and Payer ID
if(isset($_GET["token"]) && isset($_GET["PayerID"]) && isset($_GET['OID']))
{
	#we will be using these two variables to execute the "DoExpressCheckoutPayment"
	$token 		= $_GET["token"];
	$payer_id 	= $_GET["PayerID"];
	$order_id	= $_GET['OID'];
			
	#get session variables
	$paypal_product = $_SESSION["paypal_products"];
	$paypal_data 	= '';
	$ItemTotalPrice = 0;

	foreach($paypal_product['items'] as $key=>$p_item)
	{		
		$paypal_data .= '&L_PAYMENTREQUEST_0_QTY'.$key.'='. urlencode($p_item['itm_qty']);
		$paypal_data .= '&L_PAYMENTREQUEST_0_AMT'.$key.'='.urlencode($p_item['itm_price']);
		$paypal_data .= '&L_PAYMENTREQUEST_0_NAME'.$key.'='.urlencode($p_item['itm_name']);
		$paypal_data .= '&L_PAYMENTREQUEST_0_NUMBER'.$key.'='.urlencode($p_item['itm_code']);
		        
		#item price X quantity
		$subtotal = ($p_item['itm_price']*$p_item['itm_qty']);
				
		#total price
		$ItemTotalPrice = ($ItemTotalPrice + $subtotal);
	}
			
	$padata = 	'&TOKEN='.urlencode($token).
				'&PAYERID='.urlencode($payer_id).
				'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE").
				$paypal_data.
				'&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice).
				'&PAYMENTREQUEST_0_TAXAMT='.urlencode($paypal_product['assets']['tax_total']).
				'&PAYMENTREQUEST_0_SHIPPINGAMT='.urlencode($paypal_product['assets']['shippin_cost']).
				'&PAYMENTREQUEST_0_SHIPDISCAMT='.urlencode($paypal_product['assets']['discount']).
				'&PAYMENTREQUEST_0_AMT='.urlencode($paypal_product['assets']['grand_total']).
				'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode($PayPalCurrencyCode);
				
	#We need to execute the "DoExpressCheckoutPayment" at this point to Receive payment from user.
	$paypal= new MyPayPal();
	$httpParsedResponseAr = $paypal->PPHttpPost('DoExpressCheckoutPayment', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);
	

	if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
	{
		#assign transaction id and order status
		$txn_id		=	urldecode($httpParsedResponseAr["PAYMENTINFO_0_TRANSACTIONID"]);
		$payStatus	=	$httpParsedResponseAr["PAYMENTINFO_0_PAYMENTSTATUS"];				
		
		#Sometimes Payment are kept pending even when transaction is complete. 
		#hence we need to notify user about it and ask him manually approve the transiction
						
		#we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
		#GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut

		
		$padata = 	'&TOKEN='.urlencode($token);
		$paypal= new MyPayPal();
		$httpParsedResponseAr = $paypal->PPHttpPost('GetExpressCheckoutDetails', $padata, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode);

		#print_r($httpParsedResponseAr);die;
		if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) 
		{
			if($payStatus == 'Completed' && !empty($txn_id))
			{
				#get order info base on order id
				#$rsOrder = $objOrder->getInfoById($order_id);
			
				#Insert Comment
				$objOrder->InsertComment($order_id,	'',0,'','Completed', '', 1, 1, $txn_id, serialize($httpParsedResponseAr));
			
				echo	'<div style="color:green">Payment Received! Your product will be sent to you very soon!</div>';
				
				if($_GET['mod']=='web')
				{
					echo '<script>parent.location="http://m.xxxxxxxxx.com/paythanku.html";</script>';
					exit();
				}
			}
			else
			{
				# Update order status
				$objOrder->InsertComment($order_id,	'',0,'', $payStatus, '', 1, 1, $txn_id, serialize($httpParsedResponseAr));
				echo '<div style="color:red">Transaction Complete, but payment is still pending! '.
				'You need to manually authorize this payment in your <a target="_new" href="http://www.paypal.com">Paypal Account</a></div>';
			}
		}
		else
		{
			if($_GET['mod']=='web')
			{
				$objOrder->InsertComment($order_id,	'', 0,'','Canceled', '', 	1, 1, '', '');
				echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
				exit();
			}
			/*echo '<div style="color:red"><b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
			echo '<pre>';
			print_r($httpParsedResponseAr);
			echo '</pre>';
			*/
		}
	}
	else
	{
		$objOrder->InsertComment($order_id,	'', 0,'','Canceled', '', 	1, 1, '', '');
		echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
		exit();
		/*echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
		echo '<pre>';
		print_r($httpParsedResponseAr);
		echo '</pre>';
		*/
	}
}
elseif(isset($_GET["token"]) && isset($_GET['OID'])){
	
	$objOrder->InsertComment($order_id,	'', 0,'','Canceled', '', 	1, 1, '', '');
	
	echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
	exit();
}
else
{
	$order_id	= $_GET['OID'];
	
	$objOrder->InsertComment($order_id,	'', 0,'','Canceled', '', 	1, 1, '', '');
	
	echo '<script>parent.location="http://m.xxxxxxxxx.com/payfail.html";</script>';
	exit();
}
?>